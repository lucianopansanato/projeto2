package utfpr.ct.dainf.if62c.projeto;

import java.util.TimerTask;

/**
 * Linguagem Java
 * @author 
 */
public class Aviso extends TimerTask {
    
    protected final Compromisso compromisso;

    public Aviso(Compromisso compromisso) {
       this.compromisso = compromisso;
    }
    
    // A tarefa implementada pela classe Aviso deve exibir uma mensagem 
    // associada ao compromisso. Por exemplo, se a descrição do compromisso 
    // for "Reunião", a mensagem exibida deve ser "Reunião começa em 120s".
    @Override
    public void run() {
        if (compromisso.getDescricao().equals("Reunião")) {
            System.out.println("Reunião começa em 120s");
        }
    }    
}
