package utfpr.ct.dainf.if62c.projeto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;

/**
/**
 * Linguagem Java
 * @author 
 */
public class Agenda {
    private final String descricao;
    private final List<Compromisso> compromissos = new ArrayList<>();
    private final Timer timer;

    public Agenda(String descricao) {
        this.descricao = descricao;
        timer = new Timer(descricao);
    }

    public String getDescricao() {
        return descricao;
    }

    public List<Compromisso> getCompromissos() {
        return compromissos;
    }
    
    public void novoCompromisso(Compromisso compromisso) {
        compromissos.add(compromisso);
        Aviso aviso = new AvisoFinal(compromisso);
        compromisso.registraAviso(aviso);
        // com a classe Aviso devidamente implementada, o erro de compilação
        // deverá desaparecer
        timer.schedule(aviso, compromisso.getData());
    }
    
    // A antecedência é dada em segundos. Este método deve criar um aviso para 
    // o compromisso, registrá-lo na lista de avisos do compromisso e agendar 
    // a execução do aviso para ocorrer com a antecedência especificada em 
    // relação à data do compromisso.
    public void novoAviso(Compromisso compromisso, int antecedencia) {
        Aviso aviso = new Aviso(compromisso);
        compromisso.registraAviso(aviso);
        Date data = new Date(compromisso.getData().getTime() - (antecedencia * 1000));
        timer.schedule(aviso, data);
    }
    
    // A antecedência e o intervalo são dados em segundos. Este método deve 
    // criar um aviso para o compromisso, registrá-lo na lista de avisos do 
    // compromisso e agendar a execução do aviso para ocorrer a primeira vez 
    // com a antecedência especificada em relação à data do compromisso e 
    // depois a cada intervalo especificado.
    public void novoAviso(Compromisso compromisso, int antecedencia, int intervalo) {
       Aviso aviso = new Aviso(compromisso);
       compromisso.registraAviso(aviso);
       Date data = new Date(compromisso.getData().getTime() - (antecedencia * 1000));
       timer.scheduleAtFixedRate(aviso, data, (intervalo * 1000));
    }
    
    // Este método deve cancelar todos os avisos associados ao compromisso 
    // e remover o compromisso da lista de compromissos da agenda.
    public void cancela(Compromisso compromisso) {
        compromisso.getAvisos().clear();
        compromissos.remove(compromisso);
    }
    
    // Este método deve cancelar o aviso especificado e removê-lo da lista 
    // de avisos do compromisso.
    public void cancela(Aviso aviso) {
        aviso.cancel();
        aviso.compromisso.getAvisos().remove(aviso);
    }
    
    // Este método deve interromper todas as operações relacionadas à agenda 
    // de modo que o programa possa ser terminado.
    public void destroi() {
        timer.cancel();
    }
}
