package utfpr.ct.dainf.if62c.projeto;

/**
 * Linguagem Java
 * @author 
 */
public class AvisoFinal extends Aviso {

    public AvisoFinal(Compromisso compromisso) {
        super(compromisso);
    }
    
    @Override
    public void run() {
        if (compromisso.getDescricao().equals("Reunião")) {
            System.out.println("Reunião começa agora");
            this.cancel();
        }
    }    
}
